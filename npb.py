
print('---------------------NPB---------------------')

import socket
import threading

HEADER = 64

NPB_PORT = 6002
SERVER_PORT = 7001

CONN = {
    # ADDR+PORT : CODE
}
KEYS = []

# SERVER = ""
# Another way to get the local IP address automatically
SERVER = socket.gethostbyname(socket.gethostname())
print(SERVER)
print(socket.gethostname())
FORMAT = 'UTF-8'
DISCONNECT_MESSAGE = "!DISCONNECT"

NPB_ADDR = (SERVER, NPB_PORT)
SERVER_ADDR = (SERVER, SERVER_PORT)

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(NPB_ADDR)

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(SERVER_ADDR)

# when npb act as client
def sendToServer(msg, conn):
    message = msg.encode(FORMAT)
    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)
    send_length += b' ' * (HEADER - len(send_length))
    client.send(send_length)
    client.send(message)

    forwardmsg = client.recv(2048).decode(FORMAT)
    print(forwardmsg)
    conn.send(forwardmsg.encode(FORMAT))

    # send(DISCONNECT_MESSAGE)

def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")
    connected = True
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)

            if msg == DISCONNECT_MESSAGE:
                connected = False

            clientid = f'{addr[0]}:{addr[1]}'
            #check if it is new client
            if CONN.get(clientid, 'new') == 'new':
                CONN[clientid] = msg
                print(CONN)
            sendToServer(msg, conn)
            print(f"[{addr}] {msg}")
            conn.send("MSG received : npb".encode(FORMAT))

    conn.close()

def start():
    server.listen()
    print(f"[LISTENING] Server is listening on {SERVER}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")

# def check_conn_code(SEED):
#     # $[128-256]-[1024-2096]-[256-512]
#     # $160-1430-320
#
#     g1, g2, g3 = SEED[1:].split('-')
#     if (int(g1) in range(128, 257)) and (int(g2) in range(1024, 2096)) and (int(g3) in range(256, 513)):
#         return True
#     else:
#         return False

print("[STARTING] server is starting...")
start()
